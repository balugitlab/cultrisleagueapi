from aiohttp import web
from pony import orm
import models
from responses import json_response

async def create_division(request):
    name = request.match_info.get("name")
    with orm.db_session:
        new_division = models.Division(name=name)
    return json_response({"ok":"created"})


async def delete_division(request):
    pass
    return json_response({"ok":top_players})
