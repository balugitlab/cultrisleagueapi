from pony import orm
from aiohttp import web
import models
from responses import json_response


async def create_player(request):
    request = await request.json()
    with orm.db_session:
        p = models.Player(
            name=request["name"], rank=request["rank"], division=request["division"]
        )
    return json_response(player_to_dict(p))


async def get_player_by_id(request):
    id = request.match_info.get("id")
    with orm.db_session:
        p = models.Player[id]
    return json_response(player_to_dict(p))


async def get_player_by_name(request):
    name = request.match_info.get("name")
    with orm.db_session:
        p = models.Player.get(name=name)
    return json_response(player_to_dict(p))


async def update_division(request):
    request = await request.json()
    player_id = request["player_id"]
    new_division = request["new_division"]
    with orm.db_session:
        p = models.Player[player_id]
        p.division = request["new_division"]
    return json_response(player_to_dict(p))


async def update_rank(request):
    request = await request.json()
    player_id = request["player_id"]
    new_rank = request["new_rank"]
    with orm.db_session:
        p = models.Player[player_id]
        p.rank = request["new_rank"]
    return json_response(player_to_dict(p))


async def update_name(request):
    request = await request.json()
    player_id = request["player_id"]
    new_name = request["new_name"]
    with orm.db_session:
        p = models.Player[player_id]
        p.name = request["new_name"]
    return json_response(player_to_dict(p))


async def delete_player(request):
    pass


def player_to_dict(player):
    return {
        "id": player.id,
        "name": player.name,
        "rank": player.rank,
        "division_id": player.division.id,
    }
