from aiohttp import web
import models
import helpers
from pony import orm
from responses import json_response


async def get_scoreboard(request):
    division = int(request.match_info.get("division"))
    length = int(request.match_info.get("length"))

    top_players = helpers.get_top_players_by_division(length, division)
    return json_response(top_players)


async def get_recent_matches(request):
    amount = int(request.match_info.get("amount"))
    recent_matches = helpers.get_recent_matches(amount)
    return json_response(recent_matches)


async def post_scoreboard(request):
    request = await request.json()
    with orm.db_session:
        score = models.Scoreboard(
            player1_id=request["player1_id"],
            player2_id=request["player2_id"],
            player1_score=request["player1_score"],
            player2_score=request["player2_score"],
        )
    return json_response({"ok": "much wow great success"})


async def delete_match_by_id(request):
    request = await request.json()
    match_id = request["match_id"]
    helpers.delete_match_by_id(match_id)
    return json_response({"ok": "match deleted"})
