from pony import orm
import datetime, time
import os

db = orm.Database()

class Division(db.Entity):
    name = orm.Required(str)
    players = orm.Set("Player")

    @orm.db_session
    def remove_player(self, id=None, name=None):
        if id:
            player = Player[id]
            player.division = None
            return

        if name:
            player = Player.get(name=name)
            player.division = None
            return

    @orm.db_session
    def add_player(self, id=None, name=None):
        if id:
            player = Player[id]
            player.division = self.id
            return

        if name:
            player = Player.get(name=name)
            player.division = self.id
            return


class Scoreboard(db.Entity):
    # QUESTION: how to do this with Relationships? Or would that be stupid?
    # my initial thought was
    # player1 = orm.Required(Player)
    # player2 = orm.Required(Player)
    # but that throws some errors

    player1_id = orm.Required(int)
    player2_id = orm.Required(int)
    player1_score = orm.Required(int)
    player2_score = orm.Required(int)
    played = orm.Required(datetime.datetime, sql_default='CURRENT_TIMESTAMP')


class Player(db.Entity):
    name = orm.Optional(str, unique=True)
    rank = orm.Optional(int)
    division = orm.Optional(Division)

def db_init(db):
    db.bind(provider='postgres', dsn=os.environ['DATABASE_URL']) # Heroku

    # db.bind(provider='sqlite', filename='testdatabase.sqlite', create_db=True) # dev
    db.generate_mapping(create_tables=True)
    orm.set_sql_debug(True)

db_init(db)

# Examples below
# if __name__ == "__main__":

#     with orm.db_session:
    # NOTE: you can not do this all in one go it will error on you

    # Create 5 test divisions
    # d1 = Division(name="Division1")
    # d2 = Division(name="Division2")
    # d3 = Division(name="Division3")
    # d4 = Division(name="Division4")
    # d5 = Division(name="Division5")

    # create test players
    # p1 = Player(name="player1")
    # p2 = Player(name="player2", division=1)
    # p3 = Player(name="player3", division=1, rank=329)

    # add remove player from division
    # d1 = Division[1]
    # d1.add_player(name="player1")
    # d1.remove_player(name="player1")

    # Scoreboard reporting
    # p1 = Player[1]
    # p2 = Player[2]
    # p3 = Player[3]
    # score = Scoreboard(player1_id=p2.id, player2_id=p3.id, player1_score=2, player2_score=22)
