import json
from aiohttp import web

def json_response(respons_dic):
    headers = {"Access-Control-Allow-Origin": "*"}
    return web.Response(text=json.dumps(respons_dic), body=None, status=200,
                    headers=headers, content_type='application/json')
