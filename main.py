from aiohttp import web
from pony import orm
import models
import controllers.division_controller as division_controller
import controllers.player_controller as player_controller
import controllers.scoreboard_controller as scoreboard_controller
import os
from responses import json_response


def handle(request):
    resp = {"very": "noice"}
    return json_response(resp)


app = web.Application()
app.add_routes(
    [
        web.get("/", handle),
        web.post("/create_player", player_controller.create_player),
        web.get("/get_player_by_id/{id}", player_controller.get_player_by_id),
        web.get("/get_player_by_name/{name}", player_controller.get_player_by_name),
        web.post("/player_update_division", player_controller.update_division),
        web.post("/player_update_rank", player_controller.update_rank),
        web.post("/player_update_name", player_controller.update_name),
        web.get(
            "/get_scoreboard/{division}/{length}", scoreboard_controller.get_scoreboard
        ),  # length refers to the amount of players that will be on that scoreboard in ascending order
        web.get(
            "/get_recent_matches/{amount}", scoreboard_controller.get_recent_matches
        ),
        web.post("/post_scoreboard", scoreboard_controller.post_scoreboard),
        web.post("/delete_match_by_id", scoreboard_controller.delete_match_by_id),
        web.post("/create_division/{name}", division_controller.create_division),
        web.get("/end", handle),
    ]
)


# web.run_app(app, port=5555, host="127.0.0.1")  # dev
web.run_app(app, port=os.environ["PORT"])
