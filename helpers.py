from pony import orm
import models
import json


def get_top_players_by_division(n, division):
    top_players = []

    with orm.db_session:
        for player in models.Player.select().filter(
            lambda player: player.division.id == division
        ):
            player_data = {}
            player_data["id"], player_data["name"] = player.id, player.name
            player_data["wins"], player_data["losses"] = get_wins_losses_by_player_id(
                player.id
            )

            player_data["score"] = calculate_score(
                get_win_round_margin_by_player_id(player.id), player_data["losses"]
            )

            top_players.append(player_data)

    top_players = sorted(top_players, key=lambda k: k["score"], reverse=True)
    return top_players[:n]


def get_wins_losses_by_player_id(player_id):
    with orm.db_session:
        wins = len(
            orm.select(s for s in models.Scoreboard if s.player1_id == player_id)[:]
        )
        losses = len(
            orm.select(s for s in models.Scoreboard if s.player2_id == player_id)[:]
        )

    return wins, losses


def get_win_round_margin_by_player_id(player_id):
    with orm.db_session:
        wins = orm.select(s for s in models.Scoreboard if s.player1_id == player_id)[:]

        return [win.player1_score - win.player2_score for win in wins]


def calculate_score(wins_margins, losses):
    print("calc score", wins_margins, losses)
    """
    2 pts for 11-0 (shutout)
    1.5 pts for 7+
    1 for any margin
    -1 for loss
    """
    score = 0
    for margin in wins_margins:
        if margin == 11:
            score += 2
        elif margin > 6:
            score += 1.5
        else:
            score += 1
    score = score - (losses * 1.0)

    return score


def get_recent_matches(n):
    recent_matches = []

    with orm.db_session:
        matches = orm.select(s for s in models.Scoreboard)[:]

        for match in matches:
            match_data = {}
            match_data["match_id"] = match.id
            match_data["winner_id"], match_data["winner_name"] = (
                match.player1_id,
                models.Player[match.player1_id].name,
            )
            match_data["loser_id"], match_data["loser_name"] = (
                match.player2_id,
                models.Player[match.player2_id].name,
            )
            match_data["winner_score"], match_data["loser_score"] = (
                match.player1_score,
                match.player2_score,
            )
            match_data["date"] = match.played.isoformat()

            recent_matches.append(match_data)

    recent_matches = sorted(recent_matches, key=lambda k: k["date"], reverse=True)

    return recent_matches[:n]


def delete_match_by_id(match_id):
    with orm.db_session:
        models.Scoreboard[match_id].delete()


if __name__ == "__main__":

    # top = get_top_players_by_division(3, 1)
    # print(top)
    # for player in top:
    #     print(type(player))
    #     print(player)
    # # print(get_win_round_margin_by_player_id(1))

    for i in get_all_matches():
        print(i)
    # delete_match_by_id(1)

    # end
